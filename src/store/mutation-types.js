/**
 * @Author: Sam
 * @Date: 2019/11/12
 * @Version: 1.0
 **/
export const SET_SELLER = 'SET_SELLER'

export const SET_GOODS = 'SET_GOODS'

export const LOGIN = 'LOGIN'

export const LOGOUT = 'LOGOUT'

export const SET_ACCOUNT = 'SET_ACCOUNT'
