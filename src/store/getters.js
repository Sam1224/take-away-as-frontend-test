/**
 * @Author: Sam
 * @Date: 2019/11/12
 * @Version: 1.0
 **/
export const seller = state => state.seller

export const goods = state => state.goods

export const token = state => state.token

export const account = state => state.account
