import Vue from 'vue'
import Router from 'vue-router'
import goods from '@/components/goods/goods'
import ratings from '@/components/ratings/ratings'
import seller from '@/components/seller/seller'
import sellerlist from '@/components/sellerlist/sellerlist'
import sellerhome from '@/components/sellerhome/sellerhome'
import Login from '@/components/login/login'
import Register from '@/components/register/register'
import AdminHome from '@/components/admin/home/home'
import AdminIndex from '@/components/admin/index/index'
import AdminUserIndex from '@/components/admin/users/index'
import AdminUsersList from '@/components/admin/users/userslist'
import AdminAddUser from '@/components/admin/users/adduser'
import AdminEditUser from '@/components/admin/users/edituser'
import AdminSellerIndex from '@/components/admin/sellers/index'
import AdminSellersList from '@/components/admin/sellers/sellerslist'
import AdminAddSeller from '@/components/admin/sellers/addseller'
import AdminEditSeller from '@/components/admin/sellers/editseller'
import AdminEditGoods from '@/components/admin/sellers/editgoods'
import AdminEditRatings from '@/components/admin/sellers/editratings'
import AdminOrdersIndex from '@/components/admin/orders/index'
import AdminOrdersList from '@/components/admin/orders/orderslist'
import AdminAddOrder from '@/components/admin/orders/addorder'
import AdminEditOrder from '@/components/admin/orders/editorder'
import AdminCommentOrder from '@/components/admin/orders/commentorder'
import AdminLogin from '@/components/admin/login/login'
import GithubRedirect from '@/components/admin/login/githubredirect'
import GitlabRedirect from '@/components/admin/login/gitlabredirect'
import GiteeRedirect from '@/components/admin/login/giteeredirect'
import BitbucketRedirect from '@/components/admin/login/bitbucketredirect'
import WeiboRedirect from '@/components/admin/login/weiboredirect'
import store from '@/store/store'

Vue.use(Router)

const router = new Router({
  linkActiveClass: 'active',
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'sellerlist',
      component: sellerlist
    },
    {
      path: '/sellerhome',
      component: sellerhome,
      children: [
        {
          path: '',
          name: 'sellerhome',
          component: goods,
          props: true
        },
        {
          path: 'goods',
          name: 'goods',
          component: goods,
          props: true
        },
        {
          path: 'ratings',
          name: 'ratings',
          component: ratings,
          props: true
        },
        {
          path: 'seller',
          name: 'seller',
          component: seller,
          props: true
        }
      ]
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/admin',
      component: AdminHome,
      children: [
        {
          path: 'githubredirect',
          name: 'githubredirect',
          component: GithubRedirect
        },
        {
          path: 'gitlabredirect',
          name: 'gitlabredirect',
          component: GitlabRedirect
        },
        {
          path: 'giteeredirect',
          name: 'giteeredirect',
          component: GiteeRedirect
        },
        {
          path: 'bitbucketredirect',
          name: 'bitbucketredirect',
          component: BitbucketRedirect
        },
        {
          path: 'weiboredirect',
          name: 'weiboredirect',
          component: WeiboRedirect
        },
        {
          path: '',
          name: 'adminlogin',
          component: AdminLogin
        },
        {
          path: 'login',
          name: 'admin_login',
          component: AdminLogin
        },
        {
          path: 'index',
          name: 'index',
          component: AdminIndex,
          meta: {
            requireAuth: true
          }
        },
        {
          path: 'users',
          component: AdminUserIndex,
          meta: {
            requireAuth: true
          },
          children: [
            {
              path: '',
              name: 'userslist',
              component: AdminUsersList
            },
            {
              path: 'users',
              name: 'users',
              component: AdminUsersList
            },
            {
              path: 'add',
              name: 'adduser',
              component: AdminAddUser
            },
            {
              path: 'edit',
              name: 'edituser',
              component: AdminEditUser,
              props: true
            }
          ]
        },
        {
          path: 'sellers',
          component: AdminSellerIndex,
          meta: {
            requireAuth: true
          },
          children: [
            {
              path: '',
              name: 'sellerslist',
              component: AdminSellersList
            },
            {
              path: 'sellers',
              name: 'sellers',
              component: AdminSellersList
            },
            {
              path: 'add',
              name: 'addseller',
              component: AdminAddSeller
            },
            {
              path: 'edit',
              name: 'editseller',
              component: AdminEditSeller,
              props: true
            },
            {
              path: 'editgoods',
              name: 'editgoods',
              component: AdminEditGoods,
              props: true
            },
            {
              path: 'editratings',
              name: 'editratings',
              component: AdminEditRatings,
              props: true
            }
          ]
        },
        {
          path: 'orders',
          component: AdminOrdersIndex,
          meta: {
            requireAuth: true
          },
          children: [
            {
              path: '',
              name: 'orderslist',
              component: AdminOrdersList
            },
            {
              path: 'orders',
              name: 'orders',
              component: AdminOrdersList
            },
            {
              path: 'add',
              name: 'addOrder',
              component: AdminAddOrder,
              props: true
            },
            {
              path: 'edit',
              name: 'editOrder',
              component: AdminEditOrder,
              props: true
            },
            {
              path: 'comment',
              name: 'commentOrder',
              component: AdminCommentOrder,
              props: true
            }
          ]
        }
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some((r) => r.meta.requireAuth)) {
    if (store.state.token) {
      next()
    } else {
      if (localStorage.token && localStorage.getItem('account')) {
        store.state.token = localStorage.token
        store.state.account = JSON.parse(localStorage.getItem('account'))
        next()
      } else {
        next({
          path: '/admin',
          query: {redirect: to.fullPath}
        })
      }
    }
  } else {
    next()
  }
})

export default router
