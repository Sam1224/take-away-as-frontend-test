# Assignment 2 - Agile Software Practice.

[![pipeline status](https://gitlab.com/Sam1224/take-away-as-test/badges/master/pipeline.svg)](https://gitlab.com/Sam1224/take-away-as-test/commits/master)
[![coverage report](https://gitlab.com/Sam1224/take-away-as-test/badges/master/coverage.svg)](https://gitlab.com/Sam1224/take-away-as-test/badges/master/coverage.svg?job=coverage)

## Table of Contents
- [Assignment 2 - Agile Software Practice.](#assignment-2---agile-software-practice)
  * [Information](#information)
  * [Client UI.](#client-ui)
    + [Frontend UI](#frontend-ui)
    + [Backend UI](#backend-ui)
  * [E2E/Cypress testing.](#e2e-cypress-testing)
  * [Web API CI.](#web-api-ci)
  * [GitLab CI.](#gitlab-ci)

## Information

- ID: 20086454

- Name: Qianxiong Xu (Sam)

## Client UI.

- This frontend can be divided into 2 parts:
  - UI for [frontend]((#frontend-ui))
  - UI for [backend](#backend-ui)

### Frontend UI
- Login
  - Access:
    - Visit: https://takeawayapp-88d06.firebaseapp.com/login
  - Picture:
    - Login view:
      ![](./assets/frontend-ui/login.png)
      >>Allows users to login.

- Register
  - Access:
    - Visit: https://takeawayapp-88d06.firebaseapp.com/register
  - Picture:
    - Register view:
      ![](./assets/frontend-ui/register.png)
      >>Allows users to register accounts.
  
- Seller list
  - Access:
    - Visit: https://takeawayapp-88d06.firebaseapp.com/
  - Picture:
    - Seller list:
      ![](./assets/frontend-ui/sellerlist.png)
      >>Shows a seller list to user, allows users to select seller items.
    - Fuzzy search:
      ![](./assets/frontend-ui/sellerlist-fuzzysearch.png)
      >>Allows users to do fuzzy search.

- Goods page of a seller
  - Access:
    - Visit: https://takeawayapp-88d06.firebaseapp.com/
    - Click: Any seller.
  - Picture:
    - Goods list with menu:
      ![](./assets/frontend-ui/sellerhome-goods.png)
      >>Shows a list of goods.
    - Information mask:
      ![](./assets/frontend-ui/sellerhome-infomask.png)
      >>Shows a mask of basic information.
    - Shopcart:
      ![](./assets/frontend-ui/sellerhome-shopcart.png)
      >>Shows a shopcart.
    - Food detail page:
      ![](./assets/frontend-ui/sellerhome-fooddetail.png)
      >>Allows users to select food to see its details.

- Ratings page of a seller
  - Access:
    - Visit: https://takeawayapp-88d06.firebaseapp.com/
    - Click: Any seller.
    - Click: Ratings on the navigation bar in a seller page.
  - Picgure:
    - Ratings:
      ![](./assets/frontend-ui/sellerhome-ratings.png)
      >>Allows users to see the scores and ratings of a seller, filter to see good/bad ratings and switch to see ratings without contents.

- Seller infomation page of a seller
  - Access:
    - Visit: https://takeawayapp-88d06.firebaseapp.com/
    - Click: Any seller.
    - Click: Seller on the navigation bar in a seller page.
  - Picture:
    - Seller information:
      ![](./assets/frontend-ui/sellerhome-seller.png)
      >>Allows users to see the basic information of a seller.
    - Seller information - favorited:
      ![](./assets/frontend-ui/sellerhome-seller-favorited.png)
      >>Allows users to add a seller to favorite.

### Backend UI
- Login
  - Access:
    - Visit: https://takeawayapp-88d06.firebaseapp.com/admin
  - Picture:
    - Login view:
      ![](./assets/backend-ui/login.png)
      >>Allows users to login use their accounts or use 3rd party accounts to login, e.g. Google, Github, Gitlab, Gitee, Bitbucket and Weibo.

- Index page
  - Access:
    - Visit: https://takeawayapp-88d06.firebaseapp.com/admin
    - Login.
  - Picture:
    - Index page:
      ![](./assets/backend-ui/index.png)
      >>Shows a slider with delicious food.
      
- Drawer
  - Access:
    - Visit: https://takeawayapp-88d06.firebaseapp.com/admin
    - Click: The arrow on the left top corner.
  - Picture:
    - Drawer:
      ![](./assets/backend-ui/drawer.png)
      >>Shows some basic information of this app.
      
- Users list
  - Access:
    - Visit: https://takeawayapp-88d06.firebaseapp.com/admin
    - Login.
    - Click: Users on the navigation bar.
  - Picture:
    - Users list page:
      ![](./assets/backend-ui/users-list.png)
      >>Shows a table with users in the database and allows users to delete records.
    - Users list page - childrow:
      ![](./assets/backend-ui/users-list-childrow.png)
      >>Shows a childrow with the basic information of a user when '+' is clicked

- Add user page
  - Access:
    - Visit: https://takeawayapp-88d06.firebaseapp.com/admin
    - Login.
    - Click: Users on the navigation bar.
    - Click: Add User button at the bottom.
  - Picture:
    - Add user page:
      ![](./assets/backend-ui/users-add.png)
      >>Shows the add user page.
      
- Edit user page
  - Access:
    - Visit: https://takeawayapp-88d06.firebaseapp.com/admin
    - Login.
    - Click: Users on the navigation bar.
    - Click: Edit icon in a row of the users table.
  - Picture:
    - Edit user page:
      ![](./assets/backend-ui/users-edit.png)
      >>Shows the edit user page, the items of address, pay and favorite can be dynamically added or deleted.
    - Edit user page - address:
      ![](./assets/backend-ui/users-edit-address.png)
      >>Shows the add address option in which Google Map can be used to select address.
      
- Sellers list
  - Access:
    - Visit: https://takeawayapp-88d06.firebaseapp.com/admin
    - Login.
    - Click: Sellers on the navigation bar.
  - Picture:
    - Sellers list page:
      ![](./assets/backend-ui/sellers-list.png)
      >>Shows a table with sellers in the database and allows users to delete records.
    - Sellers list page - childrow:
      ![](./assets/backend-ui/sellers-list-childrow.png)
      ![](./assets/backend-ui/sellers-list-childrow-1.png)
      ![](./assets/backend-ui/sellers-list-childrow-2.png)
      >>Shows a childrow with the basic information of a seller when '+' is clicked

- Add seller page
  - Access:
    - Visit: https://takeawayapp-88d06.firebaseapp.com/admin
    - Login.
    - Click: Sellers on the navigation bar.
    - Click: Add Seller button at the bottom.
  - Picture:
    - Add seller page:
      ![](./assets/backend-ui/sellers-add.png)
      ![](./assets/backend-ui/sellers-add-1.png)
      >>Shows the add seller page, image uploading is supported, the items of supports and information can be dynamically added or deleted.
      
- Edit seller page
  - Access:
    - Visit: https://takeawayapp-88d06.firebaseapp.com/admin
    - Login.
    - Click: Sellers on the navigation bar.
    - Click: Edit icon in a row of the sellers table.
  - Picture:
    - Edit seller page:
      ![](./assets/backend-ui/sellers-edit.png)
      ![](./assets/backend-ui/sellers-edit-1.png)
      ![](./assets/backend-ui/sellers-edit-2.png)
      >>Shows the edit seller page, the uploaded images can be modified, items of supports and information can be dynamically added or deleted.

- Edit goods of a seller
  - Access:
    - Visit: https://takeawayapp-88d06.firebaseapp.com/admin
    - Login.
    - Click: Sellers on the navigation bar.
    - Click: Edit goods icon in a row of the sellers table.
  - Picture:
    - Edit goods page:
      ![](./assets/backend-ui/sellers-editgoods.png)
      ![](./assets/backend-ui/sellers-editgoods-1.png)
      >>Shows the edit goods page, image uploading is supported, the items of goods(menus) and food can be dynamically added or deleted.

- Edit ratings of a seller
  - Access:
    - Visit: https://takeawayapp-88d06.firebaseapp.com/admin
    - Login.
    - Click: Sellers on the navigation bar.
    - Click: Edit ratings icon in a row of the sellers table.
  - Picture:
    - Edit ratings page:
      ![](./assets/backend-ui/sellers-editratings.png)
      ![](./assets/backend-ui/sellers-editratings-1.png)
      >>Shows the edit ratings page, the items of ratings can be dynamically added or deleted.

- Orders list
  - Access:
    - Visit: https://takeawayapp-88d06.firebaseapp.com/admin
    - Login.
    - Click: Orders on the navigation bar.
  - Picture:
    - Orders list page:
      ![](./assets/backend-ui/orders-list.png)
      >>Shows a table with orders in the database and allows users to delete records.
    - Orders list page - childrow:
      ![](./assets/backend-ui/orders-list-childrow.png)
      ![](./assets/backend-ui/orders-list-childrow-1.png)
      >>Shows a childrow with the basic information of a order when '+' is clicked

- Add order page
  - Access:
    - Visit: https://takeawayapp-88d06.firebaseapp.com/admin
    - Login.
    - Click: Orders on the navigation bar.
    - Click: Add Order button at the bottom.
  - Picture:
    - Add order page:
      ![](./assets/backend-ui/orders-add.png)
      ![](./assets/backend-ui/orders-add-1.png)
      ![](./assets/backend-ui/orders-add-2.png)
      ![](./assets/backend-ui/orders-add-3.png)
      >>Shows the add order page, the user and seller should be selected, Google Map can be used to select address, the food list is depended on the seller selected.

- Edit order page
  - Access:
    - Visit: https://takeawayapp-88d06.firebaseapp.com/admin
    - Login.
    - Click: Orders on the navigation bar.
    - Click: Edit icon in a row of the orders table.
  - Picture:
    - Edit order page:
      ![](./assets/backend-ui/orders-edit.png)
      ![](./assets/backend-ui/orders-edit-1.png)
      >>Shows the edit order page, items of food can be dynamically added or deleted.

- Comment order page
  - Access:
    - Visit: https://takeawayapp-88d06.firebaseapp.com/admin
    - Login.
    - Click: Orders on the navigation bar.
    - Click: Comment icon in a row of the orders table.
  - Picture:
    - Comment order page:
      ![](./assets/backend-ui/orders-comment.png)
      >>Shows the comment order page.

## E2E/Cypress testing.

- Cypress dashboard is [here](https://dashboard.cypress.io/projects/hu498k/runs)
- There is `101` test cases in total.
- [cypress-file-upload](https://github.com/abramenal/cypress-file-upload) is used to support tests of image uploading.
- Deal with the `uncaught-exception` of cypress, processed in [index.js](./cypress/support/index.js).
- Define a format of writing test cases of each test file by myself, it is shown as follow:
  ![](./assets/cypress/format.png)
  >>Seperate a test into page contents and functions, almost all of my test cases refer to this format.

## Web API CI.

The Gitlab Pages of coverage report for Web API tests is [here](https://sam1224.gitlab.io/take-away-as-test/coverage/lcov-report/).
![](./assets/coverage-report/coverage-report.png)

## GitLab CI.

(Optional) State any non-standard features (not covered in the lectures or labs) of the GitLab CI platform that you utilized.
- Coverage report:
  
  An extra command is added to [.nycrc.yml](https://gitlab.com/Sam1224/take-away-as-test/blob/master/.nycrc.yml) to exclude the following 2 routers when testing, as they'd better not to be tested.
  
  - [oauth.js](https://gitlab.com/Sam1224/take-away-as-test/blob/master/routes/oauth.js):
    - It is added to the backend to provide some 3rd party oauth2 services APIs to the frontend to deal with the Cross-Site problem of the frontend directly invoking the APIs.
  - [file.js](https://gitlab.com/Sam1224/take-away-as-test/blob/master/routes/file.js):
    - It is added to support image uploading and displaying.
- The frontend ci/cd pipeline includes deploying to `firebase` instead of `surge`, the reason is as follow:
  - The redirect_uri of most 3rd party oauth2 services cannot include '#', therefore, turn on the `history mode` of vue-router to remove the '#' in urls.
  - `Surge` does not support `historyApiFallback` or to say `path rewrite` when configuring, therefore, use firebase instead. If not, the page will respond with `404` when accessing the urls directly, which is adopted in `cypress testing`.
  - Write the firebase deploy related command in [.gitlab-ci.yml](./.gitlab-ci.yml).
- Use `local-web-server` to support SPA(path rewriting) instead of httpserver.
