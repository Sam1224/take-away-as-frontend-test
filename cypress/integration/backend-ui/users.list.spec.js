/**
 * @Author: Sam
 * @Date: 2019/12/9
 * @Version: 1.0
 **/
/* eslint-disable */
let url = '/admin/login'
let apiBaseUrl = 'https://takeawayapp-sam-staging.herokuapp.com'

describe ('Test users list page of the backend ui', () => {
  before(() => {
    // Get token
    let token = null
    cy.request(`${apiBaseUrl}/token/admin`)
      .its('body')
      .then((res) => {
        expect(res.code).to.equal(0)
        expect(res.message).to.equal('Successfully login, use your token')
        token = res.token

        // Reset users
        cy.request(`${apiBaseUrl}/user`)
          .its('body')
          .then((res) => {
            expect(res.code).to.equal(0)
            res.data.forEach((user) => {
              cy.request('DELETE', `${apiBaseUrl}/user/${user._id}`)
            })
          })

        cy.fixture("users").then(users => {
          let [u1, u2, u3, u4, ...rest] = users
          let four = [u1, u2, u3, u4]
          four.forEach(user => {
            cy.request('POST', `${apiBaseUrl}/user`, user)
          })
        })

        // Reset sellers
        cy.request(`${apiBaseUrl}/seller`)
          .its('body')
          .then((res) => {
            expect(res.code).to.equal(0)
            res.data.forEach((seller) => {
              cy.request('DELETE', `${apiBaseUrl}/seller/${seller._id}`, {token: token})
            })
          })

        cy.fixture("sellers").then(sellers => {
          let [s1, s2, s3, s4, ...rest] = sellers
          let four = [s1, s2, s3, s4]
          four.forEach(seller => {
            seller.token = token
            cy.request('POST', `${apiBaseUrl}/seller`, seller)
          })
        })
      })
  })
  beforeEach(() => {
    cy.visit(url)
    cy.get('.login-table')
      .get('.username')
      .type('admin')
    cy.get('.login-table')
      .get('.password')
      .type('admin')
    cy.get('.login-table')
      .get('.submit')
      .click()
    cy.wait(3000)
    cy.get('.home')
      .get('.nav-wrapper')
      .find('.nav-item')
      .eq(1)
      .click()
    cy.wait(3000)
  })
  describe('Content', () => {
    describe('Users table', () => {
      it('shows a users table with a title, user items and a button', () => {
        cy.get('.userslist-wrapper')
          .get('.vue-title')
          .should('contain', 'User List')
        cy.get('tbody')
          .find('tr')
          .should('have.length', 4)
        cy.get('.userslist-wrapper')
          .get('.users-table')
          .get('.tab-wrapper')
          .get('.tab-item')
          .get('.add')
          .should('contain', 'Add User')
      })
    })
  })
  describe('Function', () => {
    describe('A detailed form in child row', () => {
      it('shows a child row when the symbol + is clicked', () => {
        cy.get('tbody')
          .find('tr')
          .eq(0)
          .find('td')
          .eq(0)
          .click()
        cy.get('tbody')
          .find('tr')
          .eq(1)
          .should('contain', 'Username')
        cy.get('tbody')
          .find('tr')
          .eq(1)
          .should('contain', 'Phone')
        cy.get('tbody')
          .find('tr')
          .eq(1)
          .should('contain', 'Address')
        cy.get('tbody')
          .find('tr')
          .eq(1)
          .should('contain', 'Pay')
        cy.get('tbody')
          .find('tr')
          .eq(1)
          .should('contain', 'Favorite')
      })
    })
    describe('Redirection', () => {
      it('redirects to add user page when add user is clicked', () => {
        cy.wait(1000)
        cy.get('.userslist-wrapper')
          .get('.users-table')
          .get('.tab-wrapper')
          .get('.tab-item')
          .get('.add')
          .click()
        cy.url()
          .should('contain', '/admin/users/add')
      })
      it('redirects to edit user page when edit user is clicked', () => {
        cy.wait(1000)
        cy.get('tbody')
          .find('tr')
          .eq(0)
          .find('td')
          .eq(4)
          .click()
        cy.url()
          .should('contain', '/admin/users/edit')
      })
    })
    describe('Delete', () => {
      it('successfully deletes one user when the deletion is confirmed', () => {
        cy.wait(1000)
        cy.get('tbody')
          .find('tr')
          .eq(1)
          .find('td')
          .eq(5)
          .click()
        cy.screenshot('backend-user-delete-confirm')
        cy.get('button')
          .contains('Delete')
          .click()
        cy.get('tbody')
          .find('tr')
          .should('have.length', 3)
      })
      it('leaves the list unchanged when the deletion is canceled', () => {
        cy.wait(1000)
        cy.get('tbody')
          .find('tr')
          .eq(1)
          .find('td')
          .eq(5)
          .click()
        cy.screenshot('backend-user-delete-cancel')
        cy.get('button')
          .contains('Cancel')
          .click()
        cy.get('tbody')
          .find('tr')
          .should('have.length', 3)
      })
    })
  })
})
